from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import create_taskForm
from tasks.models import Task


@login_required(login_url="/accounts/login/")
def create_task(request):
    if request.method == "POST":
        form = create_taskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = create_taskForm()

    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required(login_url="/accounts/login/")
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)

    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/show_my_tasks.html", context)
