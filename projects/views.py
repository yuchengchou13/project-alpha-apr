from django.shortcuts import render, redirect
from projects.models import Project
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from projects.forms import create_projectForm


@login_required(login_url="/accounts/login/")
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)

    context = {
        "projects": projects,
    }
    return render(request, "projects/list_projects.html", context)


@login_required(login_url="/accounts/login/")
def show_project(request, id):
    projects = Project.objects.filter(owner=request.user, id=id)
    details = Task.objects.filter(project__id=id)
    context = {
        "details": details,
        "projects": projects,
    }
    return render(request, "projects/detail_projects.html", context)


@login_required(login_url="/accounts/login/")
def create_project(request):
    if request.method == "POST":
        form = create_projectForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            form.save()
            return redirect("list_projects")
    else:
        form = create_projectForm()

    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)
