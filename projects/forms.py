from django.forms import ModelForm
from projects.models import Project


class create_projectForm(ModelForm):
    class Meta:
        model = Project
        fields = [
            "name",
            "description",
            "owner",
        ]
